# Changelog

## v3.0.0

- Remove `.sh` suffix from all scripts
- Use `${verb}-${noun}` format for all scripts
- Add `input`, `output`, and `meta` audio directories
- Rename audio scripts to use semi-reasonable names
- Rename `touchscreen/enable-atmel` to `touch/enable-touch`

## v2.0.0

- Remove Intel graphics configuration
