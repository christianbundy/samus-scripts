# Samus Scripts

These are a collection of scripts for the Chromebook Pixel (2015), codename Samus.

- Audio
  - Audio output
  - Audio input
  - Mute toggle
  - Volume control
- Brightness
  - Backlight brightness
  - Keyboard brightness
- Touchpad bugfix

All of these scripts were originally copied from https://github.com/raphael/linux-samus, which _also_ includes a Linux config and installation utilities for keyboard shortcuts and Intel graphics. This repository is meant to be a lightweight resource for runtime scripts (e.g. switching to/from speakers) _without_ a Linux config or installation utilities.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Support](#support)
- [Contributing](#contributing)

## Installation

Most of these scripts only depend on Bash, but you'll need to install [mxt-app](https://github.com/atmel-maxtouch/mxt-app) to use the touchpad bugfix.

## Usage

```console
$ tree ./scripts
scripts
├── audio
│   ├── input
│   │   ├── use-headset-input
│   │   └── use-mic-input
│   ├── meta
│   │   ├── decrease-volume
│   │   ├── enable-audio
│   │   ├── increase-volume
│   │   └── toggle-mute
│   └── output
│       ├── use-headphone-output
│       └── use-speaker-output
├── brightness
│   ├── backlight-brightness
│   └── keyboard-brightness
└── touch
    └── enable-touch

6 directories, 11 files
$ ./audio/enable-audio
numid=176,iface=MIXER,name='DAC1 MIXL DAC1 Switch'
  ; type=BOOLEAN,access=rw------,values=1
  : values=on
numid=178,iface=MIXER,name='DAC1 MIXR DAC1 Switch'
  ; type=BOOLEAN,access=rw------,values=1
  : values=on
```

## Support

Please [open an issue](https://gitlab.com/christianbundy/samus-scripts/issues/new) for support.

## Contributing

Please contribute using [Github Flow](https://guides.github.com/introduction/flow/). Create a branch, add commits, and [open a merge request](https://gitlab.com/christianbundy/samus-scripts/merge_requests/new).

